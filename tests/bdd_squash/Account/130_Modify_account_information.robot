*** Settings ***
Documentation    Modify account information
Metadata         ID                           130
Metadata         Reference                    account_003
Metadata         Automation priority          132
Metadata         Test case importance         Medium
Resource         squash_resources.resource
Library          squash_tf.TFParamService
Test Setup       Test Setup
Test Teardown    Test Teardown


*** Test Cases ***
Modify account information
    [Documentation]    Modify account information

    &{dataset} =    Retrieve Dataset

    Given I am logged in with email "${dataset}[email]" and oldPassword "${dataset}[oldpass]"
    Given I am on the "MyIdentity" page
    When I fill MyIdentity fields with gender "${dataset}[gender]" firstName "${dataset}[first]" lastName "${dataset}[last]" email "${dataset}[email]" oldPassword "${dataset}[oldpass]" newPass "${dataset}[new_password]" birthDate "${dataset}[birth]" partnerOffers "${dataset}[offer]" privacyPolicy "yes" newsletter "${dataset}[news]" gpdr "yes" and submit
    Then My personal information should be gender "${dataset}[gender]" firstName "${dataset}[first]" lastName "${dataset}[last]" email "${dataset}[email]" birthDate "${dataset}[birth]" partnerOffers "${dataset}[offer]" privacyPolicy "no" newsletter "${dataset}[news]" gpdr "no" and submit


*** Keywords ***
Test Setup
    [Documentation]    test setup
    ...                You can define the ${TEST_SETUP} variable with a keyword for setting up all your tests.
    ...                You can define the ${TEST_130_SETUP} variable with a keyword for setting up this specific test.
    ...                If both are defined, ${TEST_130_SETUP} will be run after ${TEST_SETUP}.

    ${TEST_SETUP_VALUE} =        Get Variable Value    ${TEST_SETUP}
    ${TEST_130_SETUP_VALUE} =    Get Variable Value    ${TEST_130_SETUP}
    IF    $TEST_SETUP_VALUE is not None
        Run Keyword    ${TEST_SETUP}
    END
    IF    $TEST_130_SETUP_VALUE is not None
        Run Keyword    ${TEST_130_SETUP}
    END

Test Teardown
    [Documentation]    test teardown
    ...                You can define the ${TEST_TEARDOWN} variable with a keyword for tearing down all your tests.
    ...                You can define the ${TEST_130_TEARDOWN} variable with a keyword for tearing down this specific test.
    ...                If both are defined, ${TEST_TEARDOWN} will be run after ${TEST_130_TEARDOWN}.

    ${TEST_130_TEARDOWN_VALUE} =    Get Variable Value    ${TEST_130_TEARDOWN}
    ${TEST_TEARDOWN_VALUE} =        Get Variable Value    ${TEST_TEARDOWN}
    IF    $TEST_130_TEARDOWN_VALUE is not None
        Run Keyword    ${TEST_130_TEARDOWN}
    END
    IF    $TEST_TEARDOWN_VALUE is not None
        Run Keyword    ${TEST_TEARDOWN}
    END

Retrieve Dataset
    [Documentation]    Retrieves Squash TM's datasets and stores them in a dictionary.
    ...
    ...                For instance, datasets containing 3 parameters "city", "country" and "currency"
    ...                have been defined in Squash TM.
    ...
    ...                First, this keyword retrieves parameter values from Squash TM
    ...                and stores them into variables, using the keyword 'Get Test Param':
    ...                ${city} =    Get Test Param    DS_city
    ...
    ...                Then, this keyword stores the parameters into the &{dataset} dictionary
    ...                with each parameter name as key, and each parameter value as value:
    ...                &{dataset} =    Create Dictionary    city=${city}    country=${country}    currency=${currency}

    ${email} =           Get Test Param    DS_email
    ${oldpass} =         Get Test Param    DS_oldpass
    ${gender} =          Get Test Param    DS_gender
    ${first} =           Get Test Param    DS_first
    ${last} =            Get Test Param    DS_last
    ${new_password} =    Get Test Param    DS_new_password
    ${birth} =           Get Test Param    DS_birth
    ${offer} =           Get Test Param    DS_offer
    ${news} =            Get Test Param    DS_news

    &{dataset} =    Create Dictionary    email=${email}    oldpass=${oldpass}              gender=${gender}    first=${first}
    ...                                  last=${last}      new_password=${new_password}    birth=${birth}      offer=${offer}
    ...                                  news=${news}

    RETURN    &{dataset}
