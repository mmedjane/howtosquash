*** Settings ***
Documentation    Fill the contact form
Metadata         ID                           133
Metadata         Reference                    cont_001
Metadata         Automation priority          190
Metadata         Test case importance         Low
Resource         squash_resources.resource
Library          squash_tf.TFParamService
Test Setup       Test Setup
Test Teardown    Test Teardown


*** Test Cases ***
Fill the contact form
    [Documentation]    Fill the contact form

    &{dataset} =    Retrieve Dataset

    Given I am on the ContactPage
    When I choose a topic "${dataset}[sujet]" and fill my email "${dataset}[mail]" and I fill a message "${dataset}[message]" and submit
    Then The following message "Votre message a bien été envoyé à notre équipe." is displayed


*** Keywords ***
Test Setup
    [Documentation]    test setup
    ...                You can define the ${TEST_SETUP} variable with a keyword for setting up all your tests.
    ...                You can define the ${TEST_133_SETUP} variable with a keyword for setting up this specific test.
    ...                If both are defined, ${TEST_133_SETUP} will be run after ${TEST_SETUP}.

    ${TEST_SETUP_VALUE} =        Get Variable Value    ${TEST_SETUP}
    ${TEST_133_SETUP_VALUE} =    Get Variable Value    ${TEST_133_SETUP}
    IF    $TEST_SETUP_VALUE is not None
        Run Keyword    ${TEST_SETUP}
    END
    IF    $TEST_133_SETUP_VALUE is not None
        Run Keyword    ${TEST_133_SETUP}
    END

Test Teardown
    [Documentation]    test teardown
    ...                You can define the ${TEST_TEARDOWN} variable with a keyword for tearing down all your tests.
    ...                You can define the ${TEST_133_TEARDOWN} variable with a keyword for tearing down this specific test.
    ...                If both are defined, ${TEST_TEARDOWN} will be run after ${TEST_133_TEARDOWN}.

    ${TEST_133_TEARDOWN_VALUE} =    Get Variable Value    ${TEST_133_TEARDOWN}
    ${TEST_TEARDOWN_VALUE} =        Get Variable Value    ${TEST_TEARDOWN}
    IF    $TEST_133_TEARDOWN_VALUE is not None
        Run Keyword    ${TEST_133_TEARDOWN}
    END
    IF    $TEST_TEARDOWN_VALUE is not None
        Run Keyword    ${TEST_TEARDOWN}
    END

Retrieve Dataset
    [Documentation]    Retrieves Squash TM's datasets and stores them in a dictionary.
    ...
    ...                For instance, datasets containing 3 parameters "city", "country" and "currency"
    ...                have been defined in Squash TM.
    ...
    ...                First, this keyword retrieves parameter values from Squash TM
    ...                and stores them into variables, using the keyword 'Get Test Param':
    ...                ${city} =    Get Test Param    DS_city
    ...
    ...                Then, this keyword stores the parameters into the &{dataset} dictionary
    ...                with each parameter name as key, and each parameter value as value:
    ...                &{dataset} =    Create Dictionary    city=${city}    country=${country}    currency=${currency}

    ${sujet} =      Get Test Param    DS_sujet
    ${mail} =       Get Test Param    DS_mail
    ${message} =    Get Test Param    DS_message

    &{dataset} =    Create Dictionary    sujet=${sujet}    mail=${mail}    message=${message}

    RETURN    &{dataset}
