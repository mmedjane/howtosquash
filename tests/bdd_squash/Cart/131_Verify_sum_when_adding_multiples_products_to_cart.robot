*** Settings ***
Documentation    Verify sum when adding multiples products to cart
Metadata         ID                           131
Metadata         Reference                    cart_003
Metadata         Automation priority          60
Metadata         Test case importance         High
Resource         squash_resources.resource
Test Setup       Test Setup
Test Teardown    Test Teardown


*** Test Cases ***
Verify sum when adding multiples products to cart
    [Documentation]    Verify sum when adding multiples products to cart

    &{datatables} =    Retrieve Datatables

    Given I am logged out
    When I navigate to category "vêtements"
    And I navigate to product "T-shirt imprimé colibri"
    And I select size "M"
    And I update quantity to "3"
    And I add to cart
    When I navigate to category "accessoires"
    And I navigate to product "Coussin renard"
    And I add to cart
    Then the cart contains "${datatables}[datatable_1]"


*** Keywords ***
Test Setup
    [Documentation]    test setup
    ...                You can define the ${TEST_SETUP} variable with a keyword for setting up all your tests.
    ...                You can define the ${TEST_131_SETUP} variable with a keyword for setting up this specific test.
    ...                If both are defined, ${TEST_131_SETUP} will be run after ${TEST_SETUP}.

    ${TEST_SETUP_VALUE} =        Get Variable Value    ${TEST_SETUP}
    ${TEST_131_SETUP_VALUE} =    Get Variable Value    ${TEST_131_SETUP}
    IF    $TEST_SETUP_VALUE is not None
        Run Keyword    ${TEST_SETUP}
    END
    IF    $TEST_131_SETUP_VALUE is not None
        Run Keyword    ${TEST_131_SETUP}
    END

Test Teardown
    [Documentation]    test teardown
    ...                You can define the ${TEST_TEARDOWN} variable with a keyword for tearing down all your tests.
    ...                You can define the ${TEST_131_TEARDOWN} variable with a keyword for tearing down this specific test.
    ...                If both are defined, ${TEST_TEARDOWN} will be run after ${TEST_131_TEARDOWN}.

    ${TEST_131_TEARDOWN_VALUE} =    Get Variable Value    ${TEST_131_TEARDOWN}
    ${TEST_TEARDOWN_VALUE} =        Get Variable Value    ${TEST_TEARDOWN}
    IF    $TEST_131_TEARDOWN_VALUE is not None
        Run Keyword    ${TEST_131_TEARDOWN}
    END
    IF    $TEST_TEARDOWN_VALUE is not None
        Run Keyword    ${TEST_TEARDOWN}
    END

Retrieve Datatables
    [Documentation]    Retrieves Squash TM's datatables and stores them in a dictionary.
    ...
    ...                For instance, 2 datatables have been defined in Squash TM,
    ...                the first one containing data:
    ...                | name | firstName |
    ...                | Bob  |   Smith   |
    ...                the second one containing data
    ...                | name  | firstName | age |
    ...                | Alice |   Smith   | 45  |
    ...
    ...                First, for each datatable, this keyword retrieves the values of each row
    ...                and stores them in a list, as follows:
    ...                @{row_1_1} =    Create List    name    firstName
    ...
    ...                Then, for each datatable, this keyword creates a list containing all the rows,
    ...                as lists themselves, as follows:
    ...                @{datatable_1} =    Create List    ${row_1_1}    ${row_1_2}
    ...
    ...                Finally, this keyword stores the datatables into the &{datatables} dictionary
    ...                with each datatable name as key, and each datatable list as value :
    ...                &{datatables} =    Create Dictionary    datatable_1=${datatable_1}    datatable_2=${datatable_2}

    @{row_1_1} =    Create List    Product                    Quantity    Price    Total
    @{row_1_2} =    Create List    T-shirt imprimé colibri    3           68,83
    @{row_1_3} =    Create List    Coussin renard             1           22,68
    @{row_1_4} =    Create List    91,51
    @{datatable_1} =    Create List    ${row_1_1}    ${row_1_2}    ${row_1_3}    ${row_1_4}

    &{datatables} =    Create Dictionary    datatable_1=${datatable_1}

    RETURN    &{datatables}
