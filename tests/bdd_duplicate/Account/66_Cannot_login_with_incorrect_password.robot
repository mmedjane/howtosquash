*** Settings ***
Documentation    Cannot login with incorrect password
Metadata         ID                           66
Metadata         Reference                    account_002
Metadata         Automation priority          20
Metadata         Test case importance         Low
Resource         squash_resources.resource
Test Setup       Test Setup
Test Teardown    Test Teardown


*** Test Cases ***
Cannot login with incorrect password
    [Documentation]    Cannot login with incorrect password

    Given I am on the AccountCreation page
    When I fill AccountCreation fields with gender "F" firstName "Alice" lastName "Noel" password "police" email "alice@noel4.com" birthDate "01/01/1970" acceptPartnerOffers "yes" acceptPrivacyPolicy "yes" acceptNewsletter "yes" acceptGdpr "yes" and submit
    And I sign out
    And I sign in with email "alice@noel.com" and password "poluce"
    Then The error message "Échec d'authentification" is displayed


*** Keywords ***
Test Setup
    [Documentation]    test setup
    ...                You can define the ${TEST_SETUP} variable with a keyword for setting up all your tests.
    ...                You can define the ${TEST_66_SETUP} variable with a keyword for setting up this specific test.
    ...                If both are defined, ${TEST_66_SETUP} will be run after ${TEST_SETUP}.

    ${TEST_SETUP_VALUE} =       Get Variable Value    ${TEST_SETUP}
    ${TEST_66_SETUP_VALUE} =    Get Variable Value    ${TEST_66_SETUP}
    IF    $TEST_SETUP_VALUE is not None
        Run Keyword    ${TEST_SETUP}
    END
    IF    $TEST_66_SETUP_VALUE is not None
        Run Keyword    ${TEST_66_SETUP}
    END

Test Teardown
    [Documentation]    test teardown
    ...                You can define the ${TEST_TEARDOWN} variable with a keyword for tearing down all your tests.
    ...                You can define the ${TEST_66_TEARDOWN} variable with a keyword for tearing down this specific test.
    ...                If both are defined, ${TEST_TEARDOWN} will be run after ${TEST_66_TEARDOWN}.

    ${TEST_66_TEARDOWN_VALUE} =    Get Variable Value    ${TEST_66_TEARDOWN}
    ${TEST_TEARDOWN_VALUE} =       Get Variable Value    ${TEST_TEARDOWN}
    IF    $TEST_66_TEARDOWN_VALUE is not None
        Run Keyword    ${TEST_66_TEARDOWN}
    END
    IF    $TEST_TEARDOWN_VALUE is not None
        Run Keyword    ${TEST_TEARDOWN}
    END
